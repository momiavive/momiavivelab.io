<!DOCTYPE html>
<html>
<head>
	<title>CipherText</title>

	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>

	<section class="form_wrap">

		<section class="info">
            <section class="info_title">
                <h2>ENCRIPTACIÓN<br>&<br>DESENCRIPTACIÓN<br>A E S</h2>
            </section>
            <section class="items">
                <p>CABEZA RAMIREZ JUAN ANTONIO</p>
                <p>ALGEBRA UNIVERSAL</p>
            </section>
        </section>

		<form method="get" action="" class="form_encry">
			<div class="encry_info">
				<h3>Encriptar Texto</h3>
				<input type="text" name="encrypted">
				<h3>Desencriptar Texto</h3>
				<input type="text" name="decrypted">
				<hr>
				<input type="submit" name="submit" value="Enviar">
			</div>
		</form>

		<div class="test">
		<?php

		$encText = isset($_GET['encrypted']) == null ? '': ($_GET['encrypted']);
		$decText = isset($_GET['decrypted']) == null ? '': ($_GET['decrypted']);

		encryption($encText);
		decryption($decText);

		function encryption($data){
			echo "<br><br><br>";
			echo "<b>Enctryption</b>"."<br>";
			echo "<br>"."Original message: ". $data . "<br>";
			$output = '';

			$iv = '1234567890123456'; //a simple size 16 key for initialization vector required by CBC
			$value = time(); //generating integer 
			srand($value); //creating a constant seed for the key
			$key = str_pad((string) rand() % 1000, 16, '0', STR_PAD_LEFT); //padding the random int from the left to make sure it generate a size 16 key
			$cyphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv); //encrypting an ciphertext
			$output = $data == null? '': base64_encode($cyphertext); //remove some undefined variables
			echo "decrypted CipherText: ". $output. "<br><br>"; //convert it to base64
		}

		function decryption ($text){
			echo "<br><br><br>";
			echo "<b>Deryption</b>"."<br>";
			echo "<br>". "Original message: ".$text. "<br>";

			$iv = '1234567890123456';
			$base64 = $text;
			$data = base64_decode($base64);

			for ($i=0; $i<1000; $i++){
				$key = (string) $i;
				$key = str_pad($key, 16, '0', STR_PAD_LEFT);
				$plain = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);

				if (stristr($plain, 'key')) {
					echo "<br><br><br>";
					echo "<b>Derypted CipherText: <b>"."$plain"."<br>";
					echo "<b>Deryption key: </b>"."$key";
				}

			}

		}

		?>
			
		</div>

	</section>
</body>
</html>